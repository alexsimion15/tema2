function addTokens(input, tokens){
    if (typeof input !== 'string' && ! (input instanceof String)) {
         throw Error('Invalid input');
}
    if(input.length < 6){
        throw Error('Input should have at least 6 characters')
    }
    
 for(var i=0;i<tokens.length;i++){
     if(!(tokens[i].hasOwnProperty('tokenName'))) {throw Error('Invalid array format');
     }
     if(typeof((tokens[i].tokenName)) !== 'string') throw Error('Invalid array format');
 }
 
 var output = "";
 var j=0;
 var words = input.split(" ");
 for(var i=0;i<words.length-1;i++){
     
     if(words[i].includes("...")){
         words[i]=words[i].replace("...",tokens[j].tokenName);
         output = output.concat("${",tokens[j].tokenName,"}"," ");
         j++;
     }
     
     else{
     output = output.concat(words[i]," ");
     }
 }

    if(words[words.length-1].includes("...")){
         words[i]=words[i].replace("...",tokens[j].tokenName);
         output = output.concat("${",tokens[j].tokenName,"}");
         j++;
     }

     else{
     output = output.concat(words[i]);
     }
     
 return output;
}

const app = {
    addTokens: addTokens
}

module.exports = app;


//console.log(addTokens("Subsemnatul ..., domiciliat in localitatea ..., are cnp-ul ...", [{tokenName:"subsemnatul"},{tokenName:"localitatea"},{tokenName:"cnp-ul"}])